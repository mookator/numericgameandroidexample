package com.example.michal.thefirstapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.TransformationMethod;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by michal on 18.02.16.
 */
public class TheGameActivity extends AppCompatActivity {
    
    final String COLUMN = "COLUMN";
    final String ROW = "ROW";


    public Random random = new Random();
    public ArrayList<ArrayList<Integer>> gridValues= generateGridValues();
    int answer = 0;
    Integer level = 1;
    Integer tries = 10;


    private ArrayList<ArrayList<Integer>> generateGridValues() {
        ArrayList<ArrayList<Integer>> gridVals = new ArrayList<>();
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            numbers.add(i);
        }
        for (int i=0; i<3; i++) {
            ArrayList<Integer> row = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                int rand = random.nextInt(numbers.size());
                row .add(numbers.get(rand));
                numbers.remove(rand);
            }
            gridVals.add(row);
        }
        return gridVals;
    }
    
    public Integer generateAnswer(String type, Integer number){
        int answer = 0;
        if (type.equals(ROW)){
            for (Integer num : gridValues.get(number)) {
                answer += num;
            }
        } else if(type.equals(COLUMN)){
            for(ArrayList<Integer> arr : gridValues){
                answer+= arr.get(number);
            }
        }
        return answer;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Integer value = extras.getInt("LEVEL");
            if(value!= null){
                level= Integer.valueOf(value);
            }
        }
        tries = setupTries();
        setContentView(R.layout.the_game);
        TextView textView = (TextView) findViewById(R.id.textView15);
        textView.setText(generateAnswer(COLUMN,0).toString());
        textView = (TextView) findViewById(R.id.textView25);
        textView.setText(generateAnswer(COLUMN,1).toString());
        textView = (TextView) findViewById(R.id.textView35);
        textView.setText(generateAnswer(COLUMN,2).toString());
        textView = (TextView) findViewById(R.id.textView51);
        textView.setText(generateAnswer(ROW,0).toString());
        textView = (TextView) findViewById(R.id.textView52);
        textView.setText(generateAnswer(ROW,1).toString());
        textView = (TextView) findViewById(R.id.textView53);
        textView.setText(generateAnswer(ROW,2).toString());
        drawLevel();
        drawTries();


        
    }


    public void submitAnswer(View view) {
        if(tries > 0) {
            tries--;
            GridLayout gl = (GridLayout) findViewById(R.id.gridLayout);
            tryGetAndReplace(gl, R.id.editText11);
            tryGetAndReplace(gl, R.id.editText12);
            tryGetAndReplace(gl, R.id.editText13);
            tryGetAndReplace(gl, R.id.editText21);
            tryGetAndReplace(gl, R.id.editText22);
            tryGetAndReplace(gl, R.id.editText23);
            tryGetAndReplace(gl, R.id.editText31);
            tryGetAndReplace(gl, R.id.editText32);
            tryGetAndReplace(gl, R.id.editText33);
            if (answer == 9) {
                LinearLayout llmain = (LinearLayout) findViewById(R.id.linear);
                Button button = new Button(this);
                button.setText("Next Level" );
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addButtonButtonAction(v);
                    }
                });
                llmain.addView(button);
            }
            drawTries();
        } else{
            LinearLayout llmain = (LinearLayout) findViewById(R.id.linear);
            Button button = new Button(this);
            button.setText("Game Over, back to menu");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gobackToMenu(v);
                }
            });
            llmain.addView(button);
        }
    }

    private void gobackToMenu(View v) {
        Intent intent = new Intent(this, TheGameMenuActivity.class);
        intent.putExtra("BESTLEVEL", level);
        startActivity(intent);
    }

    private void addButtonButtonAction(View v) {
        if(level!=9) {
            Intent intent = new Intent(this, TheGameActivity.class);
            intent.putExtra("LEVEL", level + 1);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, TheGameMenuActivity.class);
            intent.putExtra("GAMEFINISHED", "true");
            startActivity(intent);
        }

    }

    public void replaceInputWithAswer(GridLayout gl, EditText editText){
        String tag= editText.getTag().toString();
        Integer column = Integer.valueOf(tag.split("")[1]);
        Integer row = Integer.valueOf(tag.split("")[2]);
        Integer number = Integer.valueOf(editText.getText().toString());
        if(checkPlacement(column,row,number)) {
            gl.removeView(editText);
            TextView textView = new TextView(this);
            textView.setTag(tag);
            textView.setText(getValue(column, row));
            GridLayout.LayoutParams param =new GridLayout.LayoutParams();
            param.height = GridLayout.LayoutParams.WRAP_CONTENT;
            param.width = GridLayout.LayoutParams.WRAP_CONTENT;
            param.columnSpec = GridLayout.spec(column);
            param.rowSpec = GridLayout.spec(row);

            textView.setLayoutParams(param);
            gl.addView(textView);
            answer+=1;
        }
    }


    public boolean checkPlacement(Integer column, Integer row,  Integer number){
        return gridValues.get(row-1).get(column-1).equals(number);
    }

    public String getValue(Integer column, Integer row){
        return gridValues.get(row-1).get(column-1).toString();
    }

    public void tryGetAndReplace(GridLayout gl,int Id){
        try{
            if(findViewById(Id) instanceof EditText){
                EditText editText = (EditText) findViewById(Id);
                replaceInputWithAswer(gl,editText);
            }
        } catch (Exception e ){
            e.printStackTrace();
        }
    }

    public void  drawLevel(){
        TextView textView = (TextView) findViewById(R.id.level);
        textView.setText("Level " + level);
    }

    public void  drawTries(){
        TextView textView = (TextView) findViewById(R.id.tleft);
        textView.setText("Tries left:  " + tries);
    }

    public Integer setupTries(){
        return 10-level;
    }
}
