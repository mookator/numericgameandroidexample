package com.example.michal.thefirstapplication;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by michal on 18.02.16.
 */
public class TheGameMenuActivity extends AppCompatActivity {

    Integer bestLevel=1;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("BESTLEVEL");
            if(value!= null){
                bestLevel= Integer.valueOf(value);
            }
            value = extras.getString("GAMEFINISHED");
            if(value!= null && value.equals("true")){
                LinearLayout llmain = (LinearLayout) findViewById(R.id.llayout);
                TextView textView = new TextView(this);
                textView.setBackgroundColor(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
                textView.setText("You Beat the game");
                llmain.addView(textView);
                Button button = (Button) findViewById(R.id.button);
                button.setActivated(false);
                button = (Button) findViewById(R.id.button2);
                button.setActivated(false);
            }
        }
        setContentView(R.layout.the_game_menu);
    }

    public void goTonewGame(View view) {
        Intent intent = new Intent(this, TheGameActivity.class);
        intent.putExtra("LEVEL", 1);
        startActivity(intent);
    }

    public void loadLastGame(View view) {
        Intent intent = new Intent(this, TheGameActivity.class);
        intent.putExtra("LEVEL", bestLevel);
        startActivity(intent);
    }
}
