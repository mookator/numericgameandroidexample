package com.example.michal.thefirstapplication;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by michal on 17.02.16.
 */
public class MagicButtonActibity extends AppCompatActivity {


    Integer a =0;
    Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.magic_button_layout);
    }




    public void flip(View view) {
        LinearLayout ll = (LinearLayout) findViewById(R.id.ll1);
        LinearLayout llmain = (LinearLayout) findViewById(R.id.llmain);
        ScrollView sv = (ScrollView) findViewById(R.id.scroll);

        if (ll.getOrientation() == LinearLayout.HORIZONTAL){
            ll.setOrientation(LinearLayout.VERTICAL);
            llmain.setOrientation(LinearLayout.HORIZONTAL);
            llmain.setGravity(Gravity.CENTER_VERTICAL);
        } else if(ll.getOrientation() == LinearLayout.VERTICAL){
            ll.setOrientation(LinearLayout.HORIZONTAL);
            llmain.setOrientation(LinearLayout.VERTICAL);
            llmain.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public void addTv(View view) {
        LinearLayout llmain = (LinearLayout) findViewById(R.id.llmain);
        TextView textView = new TextView(this);
        textView.setBackgroundColor(Color.rgb(random.nextInt(256), random.nextInt(256),random.nextInt(256)));
        textView.setText("agagagsdgahdas" + a);
        llmain.addView(textView);
        a++;
    }

    public void addButton(View view) {
        view.setEnabled(false);
        LinearLayout llmain = (LinearLayout) findViewById(R.id.llmain);
        Button button = new Button(this);
        button.setText("button" + a);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addButtonButtonAction(v);
            }
        });
        llmain.addView(button);
        a++;
    }

    public void addButtonButtonAction(View view){

    }
}
